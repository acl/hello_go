package main

import "testing"

func TestAnotherFunction(t *testing.T) {
	if true == false {
		t.Error("error in another")
	}

	another := &Another {
		printer: func(format string, a ...interface{}) (n int, err error) { return 0, nil },
		value: 1,
	}

	if another.Get() != 1 {
		t.Error("another get 1 fails")
	}

	another.Set()

	if another.Get() != 9 {
		t.Error("another get 9 fails")
	}


}











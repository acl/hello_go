package main

type Another struct {
	printer func(string, ...interface{}) (int, error)
	value int
}

func (a *Another) Print() {
	a.printer("%d\n", a.value)
}

func (a *Another) Set() {
	a.value = 9
}

func (a *Another) Get() int {
	return a.value
}

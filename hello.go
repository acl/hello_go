package main

import "fmt"

func main() {

	fmt.Println("hello world")
	
	another := &Another {
		printer: fmt.Printf,
		value: 42,
	}

	another.Print()

}
